const axios = require("axios");
const fs = require("fs");
const zip = require("cross-zip");
const path = require("path");

module.exports = {
  getKeplrRelease: async () => {
    let filename;
    let downloadUrl;

    const response = await axios.get(
      "https://api.github.com/repos/chainapsis/keplr-wallet/releases"
    );
    filename = response.data[0].assets[0].name;
    downloadUrl = response.data[0].assets[0].browser_download_url;

    return {
      filename,
      downloadUrl,
    };
  },
  download: async (url, destination) => {
    const writer = fs.createWriteStream(destination);
    const result = await axios({
      url,
      method: "GET",
      responseType: "stream",
    });
    await new Promise((resolve) =>
      result.data.pipe(writer).on("finish", resolve)
    );
  },
  extract: async (file, destination) => {
    await zip.unzip(file, destination);
  },
  prepareKeplr: async () => {
    const release = await module.exports.getKeplrRelease();
    const downloadsDirectory = path.resolve(__dirname, "downloads");
    if (!fs.existsSync(downloadsDirectory)) {
      fs.mkdirSync(downloadsDirectory);
    }
    const downloadDestination = path.join(downloadsDirectory, release.filename);
    await module.exports.download(release.downloadUrl, downloadDestination);
    const keplrDirectory = path.join(downloadsDirectory, "keplr");
    await module.exports.extract(downloadDestination, keplrDirectory);
    return keplrDirectory;
  },
};
