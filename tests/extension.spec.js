const { test, expect } = require("@playwright/test");

process.env.DEBUG = "pw:browser*";

test("popup page", async ({ page }) => {
  await page.goto(`https://playwright.dev`);
  expect(await page.title()).toContain('Playwright');
});
